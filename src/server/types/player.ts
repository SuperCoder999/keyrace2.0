import { Socket } from "socket.io";

export interface Player {
    name: string;
    socket: Socket;
    room: string | null;
    ready: boolean;
    finishedS: number;
    progress: number;
    progressChars: number;
}

export interface PlayerMin {
    name: string;
    room: string | null;
    ready: boolean;
    finishedS: number;
    progress: number;
    progressChars: number;
}

export function minimizePlayer(player: Player): PlayerMin {
    return {
        name: player.name,
        room: player.room,
        ready: player.ready,
        progress: player.progress,
        progressChars: player.progressChars,
        finishedS: player.finishedS
    };
}
