import {
    StartGameCommentor,
    MiddleGameCommentor,
    FinishGameCommentor
} from "../commentor";

export interface RoomInfo {
    count: number;
    playing: boolean;
    textChoosen: boolean;
    text: string | null;
    mainTimerStarted: boolean;
    commentorsAdded: boolean;
    time: number;
    commentors: {
        start: StartGameCommentor | null;
        middle: MiddleGameCommentor | null;
        finish: FinishGameCommentor | null;
    };
}
