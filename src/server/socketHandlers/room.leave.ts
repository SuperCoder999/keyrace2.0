import { Socket, Server as IOServer } from "socket.io";
import { Player } from "../types/player";
import { RoomInfo } from "../types/room";
import { playerCondition, sendNewPlayers } from "../helpers/player";
import { sendNewRooms } from "../helpers/room";

export function onLeave(io: IOServer, socket: Socket, connected: Player[], roomsMap: Map<string, RoomInfo>): void {
    const player: Player | undefined = connected.find(playerCondition(socket));
    const index: number = connected.findIndex(playerCondition(socket));
    if (!player) return;

    const room: string | null = player.room;
    if (!room) return;

    const count: number = (<RoomInfo>roomsMap.get(room)).count;
    const newCount = count - 1;
    roomsMap.set(room, <RoomInfo>{ ...roomsMap.get(room), count: newCount });
    connected[index].room = null;

    socket.leave(room);
    sendNewRooms(io, roomsMap);
    sendNewPlayers(connected);
}

export default (socket: Socket, io: IOServer, connected: Player[], roomsMap: Map<string, RoomInfo>): void => {
    socket.on("LEAVE_ROOM", () => onLeave(io, socket, connected, roomsMap));
};
