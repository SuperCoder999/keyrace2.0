import { Socket, Server as IOServer } from "socket.io";
import { Player } from "../types/player";
import * as config from "../data/config";
import { playerCondition } from "../helpers/player";
import { sendNewPlayers } from "../helpers/player";
import { RoomInfo } from "../types/room";
import { sendNewRooms } from "../helpers/room";
import { onLeave } from "./room.leave";

import {
    CommentorManager,
    StartGameCommentor,
    MiddleGameCommentor,
    FinishGameCommentor
} from "../commentor";

export default (socket: Socket, io: IOServer, connected: Player[], roomsMap: Map<string, RoomInfo>): void => {
    socket.on("GET_PLAYERS", (): void => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        if (!player.room) return;
        sendNewPlayers(connected);
    });

    socket.on("GET_ROOMS", (): void => {
        sendNewRooms(io, roomsMap);
    });

    socket.on("SET_READY_STATUS", (isReady: boolean): void => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        const index: number = connected.findIndex(playerCondition(socket));
        if (!player.room) return;
        connected[index].ready = isReady;
        sendNewPlayers(connected);
    });

    socket.on("START_PRE_GAME_TIMER", () => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        if (!player.room) return;
        const room: RoomInfo = <RoomInfo>roomsMap.get(<string>player.room);
        
        if (!room.playing) {
            roomsMap.set(<string>player.room, <RoomInfo>{ ...room, playing: true });
            sendNewRooms(io, roomsMap);

            socket.removeAllListeners("NEXT_COMMENTOR_MESSAGE"); // To avoid memory leak

            if (!room.commentors.start) {
                room.commentors.start = <StartGameCommentor>CommentorManager.create(
                    "commentor:startgame",
                    {
                        io,
                        roomName: <string>player.room,
                        roomsMap
                    },
                    {
                        players: connected.filter((sPlayer: Player): boolean => player.room === sPlayer.room)
                    }
                );
            }

            room.commentors.start.emitGreetings();
            room.commentors.start.emitGameStart();

            let time = config.SECONDS_TIMER_BEFORE_START_GAME;

            const interval = setInterval((): void => {
                io.to(<string>player.room).emit("SET_PRE_GAME_TIMER", time);
                time--;

                if (time === -1) {
                    io.to(<string>player.room).emit("FINISH_PRE_GAME_TIMER");
                    clearInterval(interval);
                }
            }, 1000);
        }
    });

    socket.on("CHOOSE_TEXT", (text: string): void => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        if (!player.room) return;

        const room: RoomInfo = <RoomInfo>roomsMap.get(<string>player.room);
        
        if (!room.textChoosen) {
            roomsMap.set(<string>player.room, <RoomInfo>{
                ...room,
                textChoosen: true,
                text
            });
        }
    });

    socket.on("START_GAME_TIMER", () => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        if (!player.room) return;

        const room: RoomInfo = <RoomInfo>roomsMap.get(<string>player.room);
        const teammates: Player[] = connected.filter((sPlayer: Player): boolean => sPlayer.room === player.room);
        
        if (!room.mainTimerStarted) {
            roomsMap.set(<string>player.room, <RoomInfo>{ ...room, mainTimerStarted: true });
            sendNewRooms(io, roomsMap);

            const baseCommentorSettings = {
                io,
                roomName: player.room,
                roomsMap
            };

            if (!room.commentorsAdded) {
                room.commentors.middle = <MiddleGameCommentor>CommentorManager.create(
                    "commentor:middlegame",
                    baseCommentorSettings
                );

                room.commentors.finish = <FinishGameCommentor>CommentorManager.create(
                    "commentor:endgame",
                    baseCommentorSettings
                );
                
                room.commentorsAdded = true;
            }

            let time = config.SECONDS_FOR_GAME;
            io.to(<string>player.room).emit("SET_TEXT", room.text);

            const interval = setInterval((): void => {
                io.to(<string>player.room).emit("SET_GAME_TIMER", time);
                time--;
                roomsMap.set(<string>player.room, <RoomInfo>{ ...room, time });

                if (time !== 0 && time !== config.SECONDS_FOR_GAME && time % config.COMMENTOR_SAY_STATUS_INTERVAL === 0) {
                    const players: Player[] = connected.filter((sPlayer: Player): boolean => player.room === sPlayer.room);
                    room.commentors.middle!.emitPlayersStatuses(players);
                }

                const teammates: Player[] = connected.filter((sPlayer: Player): boolean => sPlayer.room === player.room);

                if (time === -1) {
                    room.commentors.finish!.emitPlayersRatingSorting(teammates);
                    onFinish(teammates, room, <string>player.room);
                    io.to(<string>player.room).emit("FINISH_GAME");
                    clearInterval(interval);
                }
            }, 1000);

            socket.removeAllListeners("KILL_GAME_TIMER"); // To avoid memory leak

            socket.on("KILL_GAME_TIMER", (): void => {
                const player: Player = <Player>connected.find(playerCondition(socket));
                if (!player.room) return;
                clearInterval(interval);
            });

            socket.on("COMPLETE_LEAVE_ROOM", () => room.commentors.finish!.emitPlayersRatingSorting(teammates));
            socket.on("disconnect", () => room.commentors.finish!.emitPlayersRatingSorting(teammates));
        }
    });

    function onFinish(players: Player[], room: RoomInfo, roomName: string): void {
        for (const player of players) {
            const index: number = connected.findIndex((sPlayer: Player): boolean => player.name === sPlayer.name);
            connected[index].ready = false;
            connected[index].progress = 0;
            connected[index].progressChars = 0;
        }

        roomsMap.set(roomName, <RoomInfo>{
            ...room,
            count: players.length,
            text: null,
            textChoosen: false,
            playing: false,
            mainTimerStarted: false,
            commentorsAdded: false,
            time: 0,
            commentors: {
                start: null,
                middle: null,
                finish: null
            }
        });

        sendNewRooms(io, roomsMap);
        sendNewPlayers(connected);
    }

    socket.on("SET_PROGRESS", (progress: number, chars: number): void => {
        const player: Player = <Player>connected.find(playerCondition(socket));
        const index: number = connected.findIndex(playerCondition(socket));
        if (!player.room) return;

        let room: RoomInfo = <RoomInfo>roomsMap.get(<string>player.room);

        if (((<string>room.text).length - chars) === 30 && room.playing) {
            room.commentors.finish!.emitLessThan30SymsRemaining(player);
        }

        connected[index].progress = progress;
        connected[index].progressChars = chars;
        sendNewPlayers(connected);

        if (progress >= 100) {
            connected[index].finishedS = config.SECONDS_FOR_GAME - room.time;
            room.commentors.finish!.emitFinishReach(player, config.SECONDS_FOR_GAME, room.time);
        }

        room = <RoomInfo>roomsMap.get(<string>player.room);

        const teammates: Player[] = connected.filter((sPlayer: Player): boolean => sPlayer.room === player.room);
        let allFinished = true;
        const finishedArray: boolean[] = teammates.map((player: Player): boolean => player.progress >= 100);

        for (const finished of finishedArray) {
            allFinished = allFinished && finished;
        }

        if (allFinished) {
            room.commentors.finish!.emitPlayersRatingSorting(teammates);
            onFinish(teammates, room, player.room);
            io.to(<string>player.room).emit("FINISH_GAME");
        }
    });

    const completeLeaveRoom: () => void = (): void => {
        const player: Player | undefined = connected.find(playerCondition(socket));
        if (!player || !player.room) return;

        const room: RoomInfo = <RoomInfo>roomsMap.get(<string>player.room);
        const roomName: string = player.room;

        onLeave(io, socket, connected, roomsMap);

        const teammates: Player[] = connected.filter((sPlayer: Player): boolean => sPlayer.room === roomName);

        let allFinished = true;
        const finishedArray: boolean[] = teammates.map((player: Player): boolean => player.progress >= 100);

        for (const finished of finishedArray) {
            allFinished = allFinished && finished;
        }

        for (const commentor of Object.values(room.commentors)) {
            if (commentor) {
                commentor.renewNamespace();
            }
        }

        if (allFinished && teammates.length > 0) {
            room.commentors.finish!.emitPlayersRatingSorting(teammates);
            onFinish(teammates, room, roomName);
            io.to(roomName).emit("FINISH_GAME");
        }
    }

    socket.on("COMPLETE_LEAVE_ROOM", completeLeaveRoom);

    socket.on("disconnect", (): void => {
        completeLeaveRoom();
        const index: number = connected.findIndex(playerCondition(socket));
        connected.splice(index, 1);
    })
};
