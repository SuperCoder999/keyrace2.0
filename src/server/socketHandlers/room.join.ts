import { Player, PlayerMin, minimizePlayer } from "../types/player";
import { Socket, Server as IOServer } from "socket.io";
import { RoomInfo } from "../types/room";
import { playerCondition, sendNewPlayers } from "../helpers/player";
import * as config from "../data/config";
import { sendNewRooms } from "../helpers/room";

function joinRoom(
    socket: Socket,
    roomName: string,
    connected: Player[],
    roomsMap: Map<string, RoomInfo>,
    io: IOServer,
    // eslint-disable-next-line @typescript-eslint/no-inferrable-types
    checkNew: boolean = false
): void {
    const player: Player = <Player>connected.find(playerCondition(socket));
    if (player.room) return;
    const index: number = connected.findIndex(playerCondition(socket));

    if (checkNew && roomsMap.has(roomName)) {
        socket.emit("ERROR", "ERRROOMEXISTS" , "This room already exists!");
        return;
    }

    socket.join(roomName);

    connected[index].room = roomName;

    const players: PlayerMin[] = connected
        .filter((player: Player): boolean => player.room === roomName)
        .map(minimizePlayer);

    const playersCount: number = roomsMap.get(roomName) ? (<RoomInfo>roomsMap.get(roomName)).count : 0;
    const newCount: number = playersCount + 1;

    roomsMap.set(roomName, <RoomInfo>{
        playing: false,
        count: newCount,
        textChoosen: false,
        text: null,
        mainTimerStarted: false,
        commentorsAdded: false,
        time: 0,
        commentors: {
            start: null,
            middle: null,
            finish: null
        }
    });

    socket.emit("JOIN_ROOM_COMPLETED", { name: roomName, players });
    sendNewPlayers(connected);

    sendNewRooms(io, roomsMap);
}

export default (socket: Socket, io: IOServer, connected: Player[], roomsMap: Map<string, RoomInfo>): void => {
    socket.on("JOIN_ROOM", (roomName: string): void => {
        if (!roomsMap.has(roomName) || (<RoomInfo>roomsMap.get(roomName)).count === config.MAXIMUM_USERS_FOR_ONE_ROOM) return;
        joinRoom(socket, roomName, connected, roomsMap, io);
    });

    socket.on("CREATE_ROOM", (roomName: string): void => {
        joinRoom(socket, roomName, connected, roomsMap, io, true);
    });
};
