export const texts = [
    "Science knows no country, because knowledge belongs to humanity, and is the torch which illuminates the world. Science is the highest personification of the nation because that nation will remain the first which carries the furthest the works of thought and intelligence. Louis Pasteur.",
    "To me there has never been a higher source of earthly honor or distinction than that connected with advances in science. Isaac Newton.",
    "To raise new questions, new possibilities, to regard old problems from a new angle, requires creative imagination and marks real advance in science. Albert Einstein.",
    "The most exciting phrase to hear in science, the one that heralds new discoveries, is not 'Eureka!' but 'That's funny...' Isaac Asimov.",
    "Aerodynamically, the bumble bee shouldn't be able to fly, but the bumble bee doesn't know it so it goes on flying anyway. Mary Kay Ash.",
    "What is a scientist after all? It is a curious man looking through a keyhole, the keyhole of nature, trying to know what's going on. Jacques Yves Cousteau.",
    "Success in creating AI would be the biggest event in human history. Unfortunately, it might also be the last, unless we learn how to avoid the risks. Stephen Hawking.",
    "My vision is for a fully reusable rocket transport system between Earth and Mars that is able to re-fuel on Mars - this is very important - so you don't have to carry the return fuel when you go there. Elon Musk."
];

export const jokeTexts = [
    "Smile, it's free therapy.",
    "Look back, and smile on perils past.",
    "Always keep your smile.",
    "Colors are the smiles of nature.",
    "Children learn to smile from their parents.",
    "A smile is the universal welcome.",
    "Humor is reason gone mad.",
    "Humor comes from self-confidence.",
    "A joke’s a very serious thing.",
    "Every day brings new choices."
];

export const jokeImages = [
    "https://image.shutterstock.com/image-vector/child-racing-funny-car-600w-1517635697.jpg",
    "https://image.shutterstock.com/image-vector/racing-car-formula-vector-cartoon-600w-1122158873.jpg",
    "https://image.shutterstock.com/image-vector/vector-cartoon-car-illustration-print-600w-684792331.jpg",
    "https://image.shutterstock.com/image-vector/racing-car-formula-vector-cartoon-600w-1122158873.jpg",
    "https://image.shutterstock.com/image-vector/cute-teddy-bear-cartoon-driving-600w-672033313.jpg",
    "https://image.shutterstock.com/image-vector/vector-cartoon-car-illustration-600w-523825993.jpg",
    "https://image.shutterstock.com/image-vector/vector-cartoon-car-illustration-ready-600w-1213006879.jpg",
    "https://image.shutterstock.com/image-vector/funny-cartoon-happy-boy-big-600w-161993570.jpg",
    "https://image.shutterstock.com/image-vector/smiling-race-car-vector-cartoon-600w-731173738.jpg",
    "https://image.shutterstock.com/image-vector/fastest-cartoon-car-illustration-vector-600w-397814395.jpg"
]
