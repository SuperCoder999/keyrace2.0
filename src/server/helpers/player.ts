import { Player, PlayerMin } from "../types/player";
import { Socket } from "socket.io";
import { minimizePlayer } from "../types/player";

// ------- Currying -------
export const playerCondition = (socket: Socket) => (player: Player): boolean => player.socket.id === socket.id;

export function sendNewPlayers(connected: Player[]): void {
    for (const suspectedPlayer of connected) {
        const players: PlayerMin[] = connected
                .filter((playerInfo: Player): boolean => suspectedPlayer.room === playerInfo.room)
                .map(minimizePlayer);

        suspectedPlayer.socket.emit("UPDATE_PLAYERS", minimizePlayer(suspectedPlayer), players);
    }
}
