import { Socket, Server as IOServer } from "socket.io";
import { RoomInfo } from "../types/room";
import * as config from "../data/config";

export function getRoomId(socket: Socket, roomsMap: Map<string, RoomInfo>): string | undefined {
    return Object.keys(socket.rooms).find((id: string): boolean => roomsMap.has(id));
}

export function sendNewRooms(io: IOServer, roomsMap: Map<string, RoomInfo>): void {
    for (const [name, info] of Array.from(roomsMap.entries())) {
        if (info.count === 0) {
            roomsMap.delete(name);
        }
    }

    io.emit("UPDATE_ROOMS", Array.from(roomsMap.entries()).filter(([name, info]: [string, RoomInfo]): boolean => {
        return (info.count < config.MAXIMUM_USERS_FOR_ONE_ROOM) && !info.playing;
    }));
}
