import { Player, PlayerMin } from "./types/player";
import { Server as IOServer, Namespace as IONamespace, Socket } from "socket.io";
import { jokeTexts as Jtexts, jokeImages as Jimages } from "./data/data";
import * as config from "./data/config";
import { getRoomId } from "./helpers/room";
import { RoomInfo } from "./types/room";

type CommentorType = StartGameCommentor | MiddleGameCommentor | FinishGameCommentor | JokesCommentorProxy;
type CommentorName = "commentor:startgame" | "commentor:middlegame" | "commentor:endgame" | "commentor:joke";

enum MessageType {
    message = "message",
    jokeImage = "jokeImage"
}

function getOrderNumber(num: number): string {
    if (num === 1) {
        return `first`;
    } else if (num === 2) {
        return `second`;
    } else if (num === 3) {
        return `third`;
    } else {
        return `${num}th`;
    }
}

// ------- Factory pattern -------
class CommentorFactory {
    public static create(
        type: CommentorName,
        baseCommentorOptions: {
            io: IOServer,
            roomName: string,
            roomsMap: Map<string, RoomInfo>
        },
        commentorOptions?: {
            players?: Player[]
        }
    ): CommentorType {
        switch (type) {
            case "commentor:startgame":
                return new StartGameCommentor(
                    baseCommentorOptions.io,
                    baseCommentorOptions.roomName,
                    baseCommentorOptions.roomsMap,
                    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                    commentorOptions!.players!
                );
            case "commentor:middlegame":
                return new MiddleGameCommentor(
                    baseCommentorOptions.io,
                    baseCommentorOptions.roomsMap,
                    baseCommentorOptions.roomName
                );
            case "commentor:endgame":
                return new FinishGameCommentor(
                    baseCommentorOptions.io,
                    baseCommentorOptions.roomsMap,
                    baseCommentorOptions.roomName
                );
            case "commentor:joke":
                return new JokesCommentorProxy(
                    baseCommentorOptions.io,
                    baseCommentorOptions.roomsMap,
                    baseCommentorOptions.roomName
                )
            default:
                throw new TypeError(`Unexpected commentor type: ${type}.`);
        }
    }
}

// ------- Facade pattern -------
class BaseCommentor {
    private namespace: IONamespace;
    // eslint-disable-next-line @typescript-eslint/no-inferrable-types
    private canEmitNextMessage: boolean = true;
    private messagesQueue: { text: string, type: MessageType }[] = [];
    private io: IOServer;
    private roomsMap: Map<string, RoomInfo>;
    protected roomName: string;

    constructor(io: IOServer, roomName: string, roomsMap: Map<string, RoomInfo>) {
        this.roomsMap = roomsMap;
        this.io = io;
        this.namespace = io.to(roomName);
        this.roomName = roomName;
        this.initSocket();
    }

    public renewNamespace(): void {
        this.namespace = this.io.to(this.roomName);
    }

    private getConnectedSockets(roomsMap: Map<string, RoomInfo>): { [id: string]: Socket } {
        const result: { [id: string]: Socket } = {};
        const allSockets: { [id: string]: Socket } = this.namespace.sockets;
        
        for (const socketID in allSockets) {
            const socket = allSockets[socketID];
            
            if (getRoomId(socket, roomsMap) === this.roomName) {
                result[socketID] = socket;
            }
        }

        return result;
    }

    private initSocket(): void {
        const sockets: { [id: string]: Socket } = this.getConnectedSockets(this.roomsMap);

        for (const socketID in sockets) {
            const socket = sockets[socketID];

            socket.on("NEXT_COMMENTOR_MESSAGE", (): void => {
                this.canEmitNextMessage = true;
                this.sendNextMessageIfExists();
            });
        }
    }

    private sendNextMessageIfExists(): void {
        const message: { text: string, type: MessageType } | undefined = this.messagesQueue.shift();

        if (message) {
            this.send(message.text, message.type);
        }
    }

    private send(message: string, type: MessageType): void {
        const sockets: { [id: string]: Socket } = this.getConnectedSockets(this.roomsMap);

        for (const ID in sockets) {
            sockets[ID].emit("SET_COMMENTOR_MESSAGE", message, type);
        }
    }

    protected emit(message: string, type: MessageType = MessageType.message): void {
        if (this.canEmitNextMessage) {
            this.send(message, type);
            this.canEmitNextMessage = false;
        } else {
            this.messagesQueue.push({ text: message, type });
        }
    }
}

class StartGameCommentor extends BaseCommentor {
    protected players: Player[];

    constructor(io: IOServer, roomName: string, roomsMap: Map<string, RoomInfo>, players: Player[]) {
        super(io, roomName, roomsMap);
        this.players = players;
    }

    public emitGreetings(): void {
        super.emit(`Greetings, friends I'm CMD CTRL OPT and today I'll comment the ${this.roomName} race.`);
        super.emit(`Players are: ${this.players.map((player: Player): string => player.name).join(", ")}`);
    }

    public emitGameStart(): void {
        super.emit("The timer started and the race will start in a few seconds!");
        super.emit("Good luck, players!");
    }
}

class MiddleGameCommentor extends BaseCommentor {
    private jokeCommentor: JokesCommentor;

    constructor(io: IOServer, roomsMap: Map<string, RoomInfo>, roomName: string) {
        super(io, roomName, roomsMap);
        this.jokeCommentor = <JokesCommentor>CommentorFactory.create("commentor:joke", { io, roomsMap, roomName });
    }

    public emitPlayersStatuses(players: Player[]): void {
        const sorted: Player[] = players.sort((player0: Player, player1: Player): number => {
            if (player0.progress < player1.progress) {
                return 1;
            } else if (player0.progress > player1.progress) {
                return -1;
            }
            
            return 0;
        });

        let delay: number = config.ONE_COMMENTOR_MESSAGE_DELAY_SECONDS * 2;

        this.emit(
            sorted
                .slice(0, Math.min(players.length, 3))
                .map((player: Player, i: number): string => `${player.name} is the ${getOrderNumber(i + 1)}`)
                .join(", ")
        );

        this.emit(`${sorted[0].name} has already reached ${Math.round(sorted[0].progress)} percent`);

        if (sorted.length > 1) {
            this.emit(`${sorted[1].name}'s progress is ${Math.round(sorted[1].progress)} percent`);
            delay += config.ONE_COMMENTOR_MESSAGE_DELAY_SECONDS;
        }

        const canSayGoodLuck: boolean = Math.random() > 0.8;

        if (canSayGoodLuck) {
            this.emit("Good luck one more time! :)");
            delay += config.ONE_COMMENTOR_MESSAGE_DELAY_SECONDS;
        }

        setTimeout(() => this.jokeCommentor.emitJoke(), delay * 1000);
    }
}

class FinishGameCommentor extends BaseCommentor {
    constructor(io: IOServer, roomsMap: Map<string, RoomInfo>, roomName: string) {
        super(io, roomName, roomsMap);
    }

    public emitFinishReach(player: Player, maxTime: number, currTime: number): void {
        this.emit(`Horray! ${player.name} has already reached the FINISH!`);
        this.emit(`${player.name}'s done this in ${maxTime - currTime} seconds`);
    }

    public emitLessThan30SymsRemaining(player: Player): void {
        this.emit(`${player.name} will finish the race soon! Less than 30 chars remaining!`);
    }

    private emitRating(players: PlayerMin[], sort: boolean): void {
        const sorted = sort ? players.sort((player0: PlayerMin, player1: PlayerMin): number => {
            if (player0.progress < player1.progress) {
                return 1;
            } else if (player0.progress > player1.progress) {
                return -1;
            } else {
                if (player0.finishedS < player1.finishedS) {
                    return -1;
                } else if (player0.finishedS > player1.finishedS) {
                    return 1;
                }
                
                return 0;
            }
        }) : players;

        this.emit(`Race is finished! Winners are:\n${
            sorted
                .slice(0, Math.min(players.length, 3))
                .map((player: PlayerMin, i: number): string => `${i + 1}: ${player.name} (${player.finishedS
                    ? `in ${player.finishedS} seconds`
                    : `didn't finish with progress ${Math.round(player.progress)} percent`
                })`)
                .join(";\n")
                + "."
        }`);
    }

    public emitPlayersRatingSorting(players: PlayerMin[]): void {
       this.emitRating(players, true);
    }

    public emitPlayersRating(players: PlayerMin[]): void {
        this.emitRating(players, false);
    }
}

interface IJokesCommentor {
    emitJoke(): void;
}

class BaseJokesCommentor extends BaseCommentor {
    public jokeTexts: string[];
    public jokeImages: string[];

    constructor(io: IOServer, roomName: string, roomsMap: Map<string, RoomInfo>) {
        super(io, roomName, roomsMap);
        this.jokeTexts = [...Jtexts];
        this.jokeImages = [...Jimages];
    }

    protected emitJokeText(): void {
        const randomJokeID: number = Math.floor(Math.random() * this.jokeTexts.length);
        const jokeText: string = this.jokeTexts[randomJokeID];
        this.emit(jokeText);
    }

    protected emitJokeImage(): void {
        const randomJokeID: number = Math.floor(Math.random() * this.jokeImages.length);
        const jokeSrc: string = this.jokeImages[randomJokeID];
        this.emit(jokeSrc, MessageType.jokeImage);
    }
}

class JokesCommentor extends BaseJokesCommentor implements IJokesCommentor {
    constructor(io: IOServer, roomsMap: Map<string, RoomInfo>, roomName: string) {
        super(io, roomName, roomsMap);
    }
    
    public emitJoke(): void {
        const canSendText: boolean = Math.random() > 0.5;

        if (canSendText) {
            this.emitJokeText();
        } else {
            this.emitJokeImage();
        }
    }
}

// --------- Proxy pattern ----------
class JokesCommentorProxy extends BaseJokesCommentor implements IJokesCommentor {
    private jokeCommentor: JokesCommentor;

    constructor(io: IOServer, roomsMap: Map<string, RoomInfo>, roomName: string) {
        super(io, roomName, roomsMap);
        this.jokeCommentor = new JokesCommentor(io, roomsMap, roomName);
    }

    public emitJoke(): void {
        // -------- Currying --------
        const sortRandom = (chance: number): (() => number) => (): number => Math.random() > chance ? 1 : -1;
        this.jokeTexts.sort(sortRandom(0.2));
        this.jokeImages.sort(sortRandom(0.7));
        this.jokeCommentor.emitJoke();
    }
}

export {
    CommentorFactory as CommentorManager,
    StartGameCommentor,
    MiddleGameCommentor,
    FinishGameCommentor
};
