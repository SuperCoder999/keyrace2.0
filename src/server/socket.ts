import { Server as IOServer, Socket } from "socket.io";
import { Player, minimizePlayer } from "./types/player";
import { RoomInfo } from "./types/room";
import gameHandler from "./socketHandlers/game";
import leaveRoomHandler from "./socketHandlers/room.leave";
import joinRoomHandler from "./socketHandlers/room.join";
import { sendNewRooms } from "./helpers/room";

function preflightData(socket: Socket, io: IOServer, connected: Player[], roomsMap: Map<string, RoomInfo>, name: string): void {
    const player: Player = <Player>connected.find((player: Player): boolean => player.name === name);
    socket.emit("CONNECTED", minimizePlayer(player));
    
    sendNewRooms(io, roomsMap);
}

function connectSocket(socket: Socket, connected: Player[], name: string): boolean {
    if (connected.find((player: Player): boolean => player.name === name && player.socket.id !== socket.id)) {
        socket.emit("ERROR", "ERRUSEREXISTS", "This username is already taken");
        socket.disconnect();
        return false;
    }

    connected.push({
        name,
        socket,
        room: null,
        ready: false,
        progress: 0,
        progressChars: 0,
        finishedS: 0
    });

    return true;
}

export default (io: IOServer): void => {
    const connected: Player[] = [];
    const roomsMap: Map<string, RoomInfo> = new Map<string, RoomInfo>();

    // eslint-disable-next-line @typescript-eslint/no-inferrable-types
    io.on("connection", (socket: Socket): void => {
        const username: string = socket.handshake.query.username;
        const successfulConnect = connectSocket(socket, connected, username);

        if (successfulConnect) {
            preflightData(socket, io, connected, roomsMap, username);
            gameHandler(socket, io, connected, roomsMap);
            leaveRoomHandler(socket, io, connected, roomsMap);
            joinRoomHandler(socket, io, connected, roomsMap);
        }
    });
};
