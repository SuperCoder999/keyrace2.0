function connectSocket(username = undefined) {
    const socket = io("", { query: { username } });

    socket.on("ERROR", (code, text) => {
        ModalManager.alert("Error", text, () => {
            if (code === "ERRUSEREXISTS") {
                sessionStorage.removeItem("username");
                window.location.replace("/login");
            }
        });
    });

    return socket;
}
