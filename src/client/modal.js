class ModalManager {
    static BG = document.getElementById("modalBG");
    static header = document.getElementById("modalHeader");
    static textElement = document.getElementById("modalText");
    static input = document.getElementById("modalInput");
    static button = document.getElementById("modalButton");

    static alert(headerText, content, onClose = () => {}) {
        this.BG.classList.remove("invisible");
        this.header.innerText = headerText;
        this.textElement.innerText = content;

        this.button.onclick = () => {
            onClose();
            this.BG.classList.add("invisible");
        }
    }

    static prompt(headerText, content) {
        return new Promise(resolve => {
            this.BG.classList.remove("invisible");
            this.header.innerText = headerText;
            this.input.classList.remove("invisible");
            this.input.focus();
            this.textElement.innerText = content;

            this.input.onkeydown = event => {
                const enter = 13;

                if (event.keyCode === enter) {
                    this.button.click();
                }
            };

            this.button.onclick = () => {
                this.BG.classList.add("invisible");
                this.input.classList.add("invisible");
                resolve(this.input.value);
                this.input.value = "";
            };
        });
    }
}
