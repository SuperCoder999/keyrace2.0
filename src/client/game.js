const username = sessionStorage.getItem("username");
const socket = connectSocket(username);

const usernameElement = document.getElementById("usernameHello");
usernameElement.innerText = `Hello, ${username}`;

let GAME_TEXT;
let CAN_START = true;

// -------------- Prewiew ----------------
function showPlayer(player, playersContainer) {
    const container = document.createElement("div");
    const status = document.createElement("div");
    const info = document.createElement("div");
    const nameText = document.createElement("h3");
    const bar = document.createElement("div");
    const barInner = document.createElement("div");
    
    container.classList.add("player");
    status.classList.add("player-status");
    bar.classList.add("progress-bar");
    barInner.classList.add("inner");

    nameText.innerText = player.name + (player.name === username ? " (me)" : "");
    barInner.style.width = player.progress + "%";
    if (player.progress >= 100) barInner.style.backgroundColor = "#0F0";
    if (player.ready) status.classList.add("ready");

    bar.append(barInner);
    info.append(nameText, bar);
    container.append(status, info);

    playersContainer.append(container);
}

function showRoom(roomName, roomInfo, roomsContainer) {
    const roomElement = document.createElement("div");
    const contentElement = document.createElement("div");
    const nameElement = document.createElement("div");
    const playersElement = document.createElement("div");
    const joinButton = document.createElement("button");

    roomElement.className = "room";
    contentElement.className = "room-content";
    nameElement.className = "room-name";
    playersElement.className = "room-players";
    joinButton.className = "button";

    nameElement.innerText = roomName;
    playersElement.innerText = `${roomInfo.count} / 5 players`;
    joinButton.innerText = "Join!";

    contentElement.append(nameElement, playersElement);
    roomElement.append(contentElement, joinButton);

    roomsContainer.append(roomElement);

    joinButton.onclick = function () {
        socket.emit("JOIN_ROOM", roomName);
    }
}

function highlightText(text, inputId) {
    const input = document.getElementById(inputId);
    let string = input.innerText;
    const index = string.indexOf(text);

    if (index >= 0) {
        string = string.substring(0, index) + 
            "<span class='highlight'>" +
            string.substring(index, index + text.length) +
            "</span>" +
            "<span class='highlight-next'>" +
            string.substring(index + text.length, index + text.length + 1) +
            "</span>" +
            string.substring(index + text.length + 1);

        input.innerHTML = string;
    }
}

// --------- Status -------------
function bindStatusButtons() {
    const readyButton = document.getElementById("ready");
    const notReadyButton = document.getElementById("notReady");

    readyButton.classList.remove("invisible");
    notReadyButton.classList.add("invisible");

    readyButton.onclick = () => {
        socket.emit("SET_READY_STATUS", true);
        readyButton.classList.add("invisible");
        notReadyButton.classList.remove("invisible");
    };

    notReadyButton.onclick = () => {
        socket.emit("SET_READY_STATUS", false);
        readyButton.classList.remove("invisible");
        notReadyButton.classList.add("invisible");
    };
}

function deleteStatusButtons() {
    const readyButton = document.getElementById("ready");
    const notReadyButton = document.getElementById("notReady");
    readyButton.classList.add("invisible");
    notReadyButton.classList.add("invisible");
}

// --------------- Room operations ----------------
function onRoomJoin(info) {
    const roomName = document.getElementById("roomName");
    roomName.innerText = info.name;

    const roomsPage = document.getElementById("roomsPage");
    const gamePage = document.getElementById("gamePage");
    roomsPage.classList.add("invisible");
    gamePage.classList.remove("invisible");

    const toRoomsButton = document.getElementById("toRooms");

    toRoomsButton.classList.remove("invisible");

    toRoomsButton.onclick = () => {
        leaveRoom();
    };

    bindStatusButtons();

    socket.emit("GET_PLAYERS");
    socket.on("UPDATE_PLAYERS", onUpdatePlayers);
}

function leaveRoom() {
    const roomsPage = document.getElementById("roomsPage");
    const gamePage = document.getElementById("gamePage");
    roomsPage.classList.remove("invisible");
    gamePage.classList.add("invisible");
    socket.emit("COMPLETE_LEAVE_ROOM");

    socket.removeAllListeners();

    socket.on("UPDATE_ROOMS", onUpdateRooms);
    socket.on("JOIN_ROOM_COMPLETED", onRoomJoin);
    socket.on("UPDATE_PLAYERS", onUpdatePlayers);

    const synthesis = window.speechSynthesis || window.mozspeechSynthesis || window.webkitspeechSynthesis;
    synthesis.cancel();
}

// --------- Game operations -----------
let currMe;
let currPlayers;

function onUpdatePlayers(me, players) {
    const playersContainer = document.getElementById("playersContainer");
    playersContainer.innerHTML = "";

    for (const player of players) {
        showPlayer(player, playersContainer);
    }

    currMe = me;
    currPlayers = players;

    maybeStartGame(players);
}

function onCommentorMessage(message, type) {
    const messageContainer = document.getElementById("commentorMessage");

    if (type === "message") {
        messageContainer.innerText = message;
        const synthesis = window.speechSynthesis || window.mozspeechSynthesis || window.webkitspeechSynthesis;
        const utterance = new SpeechSynthesisUtterance(message);

        if (synthesis) {
            synthesis.speak(utterance);
        }
    } else if (type === "imageJoke") {
        messageContainer.innerHTML = "";
        const img = document.createElement("img");
        img.src = message;
        img.alt = "Joke image";
        messageContainer.append(img);
    }

    const COMMENTOR_MESSAGE_DELAY = 3000;

    if (currMe.name === currPlayers[0].name) { // Only one player can send message
        setTimeout(() => socket.emit("NEXT_COMMENTOR_MESSAGE"), COMMENTOR_MESSAGE_DELAY);
    }
}

function maybeStartGame(players) {
    let allPlayersReady = true;

    for (const player of players) {
        allPlayersReady = allPlayersReady && player.ready;
    }

    if (allPlayersReady && CAN_START) {
        CAN_START = false;
        startGame();
    }
}

function setPreGameTimer(value) {
    const timer = document.getElementById("timer");
    timer.innerText = value;
}

function startGame() {
    prepareGameStart();
    socket.emit("START_PRE_GAME_TIMER");
    socket.on("SET_PRE_GAME_TIMER", setPreGameTimer);
    socket.on("FINISH_PRE_GAME_TIMER", startRealGame);

    const randomID = Math.floor(Math.random() * 8);

    fetch(`${window.location.origin}/game/texts/${randomID}`)
        .then(res => res.json())
        .then(res => {
            socket.emit("CHOOSE_TEXT", res.data);
        });
}

function prepareGameStart() {
    const messageContainer = document.getElementById("commentorMessage");
    messageContainer.innerText = "";
    window.speechSynthesis.cancel();

    const toRoomsButton = document.getElementById("toRooms");
    toRoomsButton.classList.add("invisible");

    const timer = document.getElementById("timer");
    timer.classList.remove("invisible");

    deleteStatusButtons();
}

function setGameTimer(value) {
    const timer = document.getElementById("timeLeft");
    timer.innerText = value;
}

function setGameText(value) {
    const textElement = document.getElementById("text");
    textElement.innerText = value;
    GAME_TEXT = value;

    let completedLength = 0;
    let completedPercent = 0;
    const etalonLength = GAME_TEXT.length;

    const keyboardAudio = new Audio("/keyboard_sound.mp3");
    keyboardAudio.playbackRate = 5;

    const OnKeyDown = event => {
        const suspectedLetter = event.key;
        const etalonLetter = GAME_TEXT[completedLength];

        if (completedLength < etalonLength) {
            if (etalonLetter === suspectedLetter) {
                keyboardAudio.play();
                completedLength++;
                completedPercent = completedLength / etalonLength * 100;
                highlightText(GAME_TEXT.substring(0, completedLength), "text");
                socket.emit("SET_PROGRESS", completedPercent, completedLength);
            }
        }
    };

    document.onkeydown = OnKeyDown;
}

function onGameFinish() {
    document.onkeydown = null;

    const timeLeft = document.getElementById("timeLeft");
    timeLeft.innerText = "";
    timeLeft.classList.add("invisible");

    const timer = document.getElementById("timer");
    timer.innerText = "";

    const gameContent = document.getElementById("gameContent");

    socket.emit("KILL_GAME_TIMER");

    timeLeft.classList.remove("invisible");
    gameContent.classList.add("invisible");

    const toRoomsButton = document.getElementById("toRooms");
    toRoomsButton.classList.remove("invisible");

    bindStatusButtons();
    socket.removeAllListeners();

    socket.on("SET_COMMENTOR_MESSAGE", onCommentorMessage);
    socket.on("UPDATE_ROOMS", onUpdateRooms);
    socket.on("JOIN_ROOM_COMPLETED", onRoomJoin);
    socket.on("UPDATE_PLAYERS", onUpdatePlayers);

    CAN_START = true;
    GAME_TEXT = "";
}

function startRealGame() {
    prepareRealGameStart();
    socket.emit("START_GAME_TIMER");
    socket.on("SET_GAME_TIMER", setGameTimer);
    socket.on("SET_TEXT", setGameText);
    socket.on("FINISH_GAME", onGameFinish);
}

function prepareRealGameStart() {
    const timer = document.getElementById("timer");
    timer.classList.add("invisible");

    const gameContent = document.getElementById("gameContent");
    gameContent.classList.remove("invisible");
}

function onUpdateRooms(rooms) {
    const roomsContainer = document.getElementById("roomsContainer");
    roomsContainer.innerHTML = "";

    for (const [name, roomInfo] of rooms) { // NO VAR HERE!
        showRoom(name, roomInfo, roomsContainer);
    }
}

// ------------ Binds -------------
socket.emit("GET_ROOMS");
socket.on("SET_COMMENTOR_MESSAGE", onCommentorMessage);
socket.on("UPDATE_ROOMS", onUpdateRooms);
socket.on("JOIN_ROOM_COMPLETED", onRoomJoin);

const addRoomButton = document.getElementById("addRoomButton");

addRoomButton.onclick = () => {
    ModalManager
        .prompt(
            "New room",
            "Enter the new room's name"
        )
        .then(name => {
            if (name) {
                socket.emit("CREATE_ROOM", name);
            }
        });
};

window.onbeforeunload = leaveRoom;


const logoutButton = document.getElementById("logoutButton");

logoutButton.onclick = () => {
    sessionStorage.removeItem("username");
    window.location.replace("/login");
}
