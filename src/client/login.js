const loginForm = document.getElementById("loginForm");
const input = document.getElementById("usernameInput");

if (sessionStorage.getItem("username")) {
    window.location.replace("/game");
}

loginForm.onsubmit = () => {
    sessionStorage.setItem("username", input.value);
    window.location.replace("/game");
    return false;
};
